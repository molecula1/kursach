import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Service implements Methods {

    private List<String> goods = new ArrayList<>();
    private File file;

    public Service(String pathName) {
        file = new File(pathName);
    }

    @Override
    public void open() throws IOException {
        Desktop desktop = Desktop.getDesktop();
        desktop.open(file);
    }

    @Override
    public void addGood(String good) {
        try {
            Writer output = new BufferedWriter(new FileWriter(file, true));
            output.append(checkGoodNameCorrection(good) + "\n");
            output.close();
            goods.add(good);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void parseFileInDischarge() {
        try {
            List<String> list = new ArrayList<>();
            Stream<String> stream = Files.lines(Paths.get("./Source/Data.csv"), Charset.forName("windows-1251"));
            stream.forEach(line -> list.add(line));
            FileWriter writer = new FileWriter(file.getAbsolutePath());
            for(String str : list) {
                writer.write(str + "\n");
                goods.add(str);
            }
            writer.flush();
            writer.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void openDiagram() throws IOException {
        File pic = new File("./Source/Dia.jpg");
        Desktop desktop = Desktop.getDesktop();
        desktop.open(pic);
    }

    @Override
    public void deleteLastGood() {
        try {
            goods.remove(goods.size() - 1);
            FileWriter writer = new FileWriter(file.getAbsolutePath());
            for(String str : goods) {
                writer.write(str + "\n");
            }
            writer.close();
            writer.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getListCountElements() {
        return goods.size();
    }

    private String checkGoodNameCorrection(String good) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] nameElements = good.split(";");
        if(nameElements.length < 2) {
            System.out.println("Invalid format");
        }
        if(!nameElements[0].replaceAll("[А-Яа-я]", "").trim().isEmpty()) {
            stringBuilder.append("Invalid data;");
        } else {
            stringBuilder.append(nameElements[0] + ";");
        }

        if(!nameElements[1].replaceAll("[0-9]", "").trim().isEmpty()) {
            stringBuilder.append("0");
        } else {
            stringBuilder.append(nameElements[1] + ";");
        }

        String correctedGood = stringBuilder.toString();
        System.out.println(correctedGood);

        return correctedGood;
    }

    public String getPath() {
        return file.getAbsolutePath();
    }
}



