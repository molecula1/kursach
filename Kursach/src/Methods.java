import java.io.IOException;
import java.util.List;

public interface Methods {

    public void open() throws IOException;

    public void addGood(String good);

    public void parseFileInDischarge();

    public void openDiagram() throws IOException;

    public void deleteLastGood();

    public int getListCountElements();
}
