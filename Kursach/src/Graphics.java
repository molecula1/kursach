import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class Graphics extends JFrame {

    Service service = new Service("./Source/Data.csv");

    public Graphics() {
        JFrame frame = new JFrame("Курсовая работа");
        frame.setSize(600, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridLayout(4, 1, 10, 10));
        JLabel label2 = new JLabel();
        JTextField jTextField = new JTextField(30);


        JButton Button1 = new JButton("Открыть файл");
        Button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    service.open();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        JButton Button2 = new JButton("Заполнить файл данными");
        Button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                service.parseFileInDischarge();
            }
        });
        JButton Button3 = new JButton("Добавить товар");
        Button3.addActionListener(new ButtonActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFrame frame3 = new JFrame();
                frame3.setLayout(new GridLayout());
                frame3.setSize(200, 50);
                frame3.setLocationRelativeTo(null);
                frame3.add(jTextField);
                JButton ConfirmButton = new JButton("Добавить");
                ConfirmButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        service.addGood(jTextField.getText());
                        String listCountElements = Integer.toString(service.getListCountElements());
                        label2.setText("Товаров в файле: " + listCountElements);
                    }
                });
                frame3.add(ConfirmButton);
                frame3.setVisible(true);
            }
        });
        JButton Button4 = new JButton("Диаграмма");
        Button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    service.openDiagram();
                }catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        });

        JButton Button5 = new JButton("Удалить товар");
        Button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                service.deleteLastGood();
            }
        });

        frame.add(Button1);
        frame.add(Button2);
        frame.add(Button3);
        frame.add(Button4);
        frame.add(Button5);
        frame.add(label2);

        frame.setVisible(true);
    }

    public class ButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {

        }
    }
}

